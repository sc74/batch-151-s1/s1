const {assert} = require('chai')
const {newUser} = require('../index.js')

describe('Test newUser object', ()=>{
	it('Assert newUser type is object',()=>{
		assert.equal(typeof(newUser), 'object')
	});
	it('Assert newUser.email is type string',()=> {
		assert.equal(typeof(newUser.email),'string')
	});
	it('Assert newUser.email is not undefined',()=>{
		assert.notEqual(typeof(newUser.email), 'undefined')
	});
	it('Assert that newUser.password type is string', ()=> {
		assert.equal(typeof(newUser.password), 'string')
	});
	it('Assert that password length is 16 characters long', ()=>{
		assert.isAtLeast(newUser.password.length, 16)
	});
	it('Assert that newUser newUser.firstName type is string', ()=>{
		assert.equal(typeof(newUser.firstName), 'string')
	});
	it('Assert that newUser newUser.lastName type is string', ()=>{
		assert.equal(typeof(newUser.lastName), 'string')
	});
	it('Assert that newUser newUser.firstName type is not underfined', ()=>{
		assert.notEqual(typeof(newUser.firstName), 'undefined')
	});
	it('Assert that newUser newUser.lastName type is not underfined', ()=>{
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	});
	it('Assert that newUser newUser.age is at least 18', ()=>{
		assert.isAtLeast(newUser.age, 18)
	});
	it('Assert that newUser newUser.age type is number', ()=>{
		assert.equal(typeof(newUser.age), 'number')
	});
	it('Assert that newUser newUser.contactNumber type is string', ()=>{
		assert.equal(typeof(newUser.contactNumber), 'string')
	});
	it('Assert that newUser newUser.batchNumber type is number', ()=>{
		assert.equal(typeof(newUser.batchNumber), 'number')
	});
	it('Assert that newUser newUser.batchNumber is not undefined', ()=>{
		assert.notEqual(typeof(newUser.batchNumber), 'undefined')
	});
});