let express = require ("express");
const PORT = 5000;
let app = express();

app.use(express.json());

const newUser = {
	firstName: 'John',
	lastName: 'Dlc',
	age: 18,
	contactNumber: '09123456789',
	batchNumber: 151,
	email: 'john.dlc@mail.com',
	password: 'iAmJohn1234567890'
}

module.exports = {
	newUser: newUser
}
app.listen(PORT, ()=>{
	console.log(`Server running at port ${PORT}`)
});